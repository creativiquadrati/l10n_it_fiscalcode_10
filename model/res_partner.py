# -*- coding: utf-8 -*-
# Copyright 2014 Associazione Odoo Italia (<http://www.odoo-italia.org>)
# Copyright 2016 Andrea Gallina (Apulia Software)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


class ResPartner(models.Model):
    _inherit = 'res.partner'

    @api.constrains('fiscalcode', 'individual')
    def check_fiscalcode(self):
        for partner in self:
            if partner.fiscalcode and len(partner.fiscalcode) != 16 and partner.individual:
                raise ValidationError(_("The fiscal code doesn't seem to be correct."))


    fiscalcode = fields.Char(
        'Fiscal Code', size=16, help="Italian Fiscal Code")
    individual = fields.Boolean(
        'Individual', default=False,
        help="If checked the C.F. is referred to a Individual Person")


    @api.model
    def _commercial_fields(self):
        ''' Method already declared through super in the following modules: purchase, account, product, base.
        Whenever a create or write method is invoked over a res.partner record
        eith a parent_id or child_ids, this method returns a res.partner's
        fields dictionary to be written over the parent or children records.
        Only children records with
            1) company_type = 'company'
            2) parent_id valued
        would be affected by the vat and fiscalcode overwriting
        '''
        return super(ResPartner, self)._commercial_fields() + ['fiscalcode']
